
This module allows you to set dynamic and static meta description tag to specific path.
It lets you add custome path and a pattern on how to build description meta tag.

Features
------------------------------------------------------------------------------

* You can set static meta description on a specific path on your site.

* You can set description to be available when your path have argument at the end of it.

* You can set a pattern on how to build meta description dynamicly based on node's data on your database.

* You can use path argument to find the nodes and then using nodes data to build the meta description tag.

Credits / Contact
------------------------------------------------------------------------------
The author of this module is sadegh hosseini