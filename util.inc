<?php
 /**
 * @file
 * The Utility methods
 */
 
function startswith($haystack, $needle) {
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

function get_current_url(){
    $current_path = explode('=', drupal_get_destination());
    // Extracting URL from $current_path
    if(is_array($current_path) && count($current_path) >= 2) {
        if(trim($current_path[1]) != ''){
            $current_url_full = htmlspecialchars( urldecode($current_path[1]) );

            // Removing query string
            $current_url_elements = explode('?', $current_url_full);
    }}    


    //drupal_set_message($fe[0]);

    return $current_url_elements[0];
}

// by SEO Header
function clean_text($text, $op = 'title') {

    if (!$text) {
        return;
    }

    // Determine the maximum length based on recommedations
    // by SEO MOZ (http://www.seomoz.org/blog/the-web-developers-seo-cheat-sheet)
    switch ($op) {
        case 'title': $max_length = 70; break;
        case 'description': $max_length = 155; break;
    }

    // Remove all line breaks
    $text = preg_replace('/\s+/', ' ', $text);

    // For meta descriptions, remove quotes
    if ($op == 'description') {
        $text = str_replace('"', '', $text);
    }
                  
    // Truncate to the appropriate lenght
    $text = truncate_utf8($text, $max_length, TRUE, TRUE);

    return $text;

}
