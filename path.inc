<?php
 /**
 * @file
 * The path helper method
 */


    function remove($id) {
        db_query("delete from {itag} where id = %d", $id);
    }

    function show($id) {
        $retVal='';
        $result = db_query("SELECT * from {itag} where id = %d", $id);
        $page = db_fetch_object($result);


        $retVal .= '<tr> <td> ' . ($page->path == '<front>' ? t('Front Page') : $page->path )       . '</td>';
        $retVal .= '<td> '      . $page->description  . '</td>';
        $retVal .= '<td> '      . ($page->type == 0 ? t('dynamic') : t('static') )         . '</td>';
        $retVal .= '<td> <a href="/admin/itag/edit/'. $page->id. '">'.t('edit').'</a>';
        $retVal .= ' <a href="/admin/itag/remove/'. $page->id. '">'.t('remove').'</a></td></tr>';

        return $retVal;
    }


        function get_by_url($url, $arg = 0) {
        if ($arg == 0) {
        $result = db_query("SELECT * from {itag} where path = '%s'", $url);
        return db_fetch_object($result);
        }
        else {
        $result = db_query("SELECT * from {itag} where arg = 1 and path = '%s'", $url);
        return db_fetch_object($result);     
        }
    }

    
            function get_by_id($id) {
        $result = db_query("SELECT * from {itag} where id = %d",$id);
        return db_fetch_object($result);
    }
    

    function insert($path, $descr, $type, $arg, $static) {

        db_query("INSERT INTO {itag} ( path, description, type,arg,static) VALUES  ('%s','%s',%d,%d,'%s')", $path, $descr, $type, $arg, $static);
    }


    function all() {

        $retVal = '<table>';
        
        // header
        $retVal .= '<tr> <td><b> ' . t('Path')    . '</b></td>';
        $retVal .= '<td><b> '      . t('Descr')   . '</b></td>';
        $retVal .= '<td><b> '      . t('Type')    . '</b></td>';
        $retVal .= '<td><b> '      . t('Operations')    . '</b></td></tr>';
        
        $result = db_query('SELECT id FROM {itag}');
        while ($page = db_fetch_object($result)) {
            $retVal = $retVal . show($page->id);
        }
        $retVal .= '</table>';
        return $retVal;
    }




    function update($id, $path, $descr, $type, $arg, $static) {
        db_query("update {itag} set path = '%s',description = '%s',type = %d,arg = %d,static = '%s' where id = %d", $path, $descr, $type, $arg, $static, $id);
    }
